import {Component, OnInit, ViewChild} from '@angular/core';
import * as PIXI from 'pixi.js';
import { PixiDirective } from './directives/pixi.directive';
import {isNgContainer} from "@angular/compiler";


@Component({
    selector: 'my-app',
    templateUrl: './app.component.html',
    styleUrls: [ './app.component.css' ]
})

export class AppComponent implements OnInit {

    @ViewChild(PixiDirective, {static: true})
    _canvas: PixiDirective;

    ngOnInit() {
       
        if (this._canvas !== undefined)
        {
            // this.onAssetsLoaded();
            this._canvas.apps.loader.load( () => this.onAssetsLoaded() );
        }


    }

    onAssetsLoaded() {

        let texture = PIXI.Texture.from('../public/images/panda1.png');
        let carrotTex = PIXI.Texture.from('../public/images/leaf1.png');
        let container = new PIXI.Container();
        let background = new PIXI.Graphics();
        let panda = new PIXI.Sprite(texture);

        let mousePosition: PIXI.Point;
        let bulletSpeed = 10/60;
        let bullets: PIXI.Sprite[] = [];
        let b: number;
        panda.anchor.x = 0.5;
        panda.anchor.y = 0.5;

        // panda.x = canvas.screenWidth / 2;
        // panda.y = canvas.screenHeight / 2;
        panda.position.x = this._canvas.screenWidth / 2;
        panda.position.y = this._canvas.screenHeight / 2;


        background.beginFill(0xF08080);
        background.drawRect(0,0, this._canvas.screenWidth, this._canvas.screenHeight);
        background.endFill();

        this._canvas.apps.renderer.resize(this._canvas.screenWidth, this._canvas.screenHeight);

// background.x = (app.screen.width - background.width) ;
// background.y = (app.screen.height - background.height) ;

        container.addChild(background);

        container.addChild(panda);


        container.hitArea = this._canvas.apps.screen;
        container.interactive = true;

        this._canvas.apps.stage.addChild(container);

        mousePosition = this._canvas.apps.renderer.plugins.interaction.mouse.global;

        container.on('mousemove', function() {
           panda.rotation = rotateToPoint(mousePosition.x, mousePosition.y, panda.position.x, panda.position.y);
        });

        container.on("mousedown", function(){

            shoot(panda.rotation, {
                x: panda.position.x+Math.cos(panda.rotation)*20,
                y: panda.position.y+Math.sin(panda.rotation)*20
            });

        });


        this._canvas.apps.ticker.add(function(delta) {
            panda.x += Math.cos(panda.rotation) * delta;
            panda.y += Math.sin(panda.rotation) * delta;
        });


        this._canvas.apps.renderer.render(container);
        animate();

        function animate() {
            requestAnimationFrame(animate);

            // just for fun, let's rotate mr rabbit a little
            panda.rotation = rotateToPoint(mousePosition.x, mousePosition.y, panda.position.x, panda.position.y);

            for(b=bullets.length-1;b>=0;b--){

                bullets[b].position.x += Math.cos(bullets[b].rotation)*bulletSpeed;
                bullets[b].position.y += Math.sin(bullets[b].rotation)*bulletSpeed;
            }
            // render the container

        }

        function shoot(rotation: number, param2: { x: number; y: number }) {
            let bullet = new PIXI.Sprite(carrotTex);
            bullet.position.x = param2.x;
            bullet.position.y = param2.y;
            bullet.rotation = rotation;
            container.addChild(bullet);
            bullets.push(bullet);
        }

        function rotateToPoint(x: number, y: number, x2: number, y2: number) {
            let dist_Y;
            dist_Y = y -y2;
            let dist_X;
            dist_X = x - x2;
            let angle;
            angle = Math.atan2(dist_Y, dist_X);
            //var degrees = angle * 180/ Math.PI;
            return angle;
        }


    }


}

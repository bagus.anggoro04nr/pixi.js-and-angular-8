import * as PIXI from 'pixi.js';
import * as $ from 'jquery';

// platform imports
import { Directive
    , AfterViewInit
    , ElementRef
} from '@angular/core';

@Directive({
    selector: '.pixi'
})

export class PixiDirective implements AfterViewInit
{
    // static PIXI options
    protected static OPTIONS: Object = {
        backgroundColor: 0xEBEBEB,
        antialias: true,
        transparent: true,
    };

    protected _container: HTMLDivElement;

    // PIXI app and stage references
    protected _app: PIXI.Application;
    protected _stage: PIXI.Container;
    protected baseWidth = window.innerWidth;
    protected baseHeight = window.innerHeight;
    protected div = $("div");
    protected aspectRatio = this.baseWidth/this.baseHeight;

    constructor(public _elRef: ElementRef)
    {
        this._container = <HTMLDivElement> this._elRef.nativeElement;

        const options = Object.assign({width: this.baseWidth, height: this.baseHeight},
            PixiDirective.OPTIONS);

        this._app = new PIXI.Application(options);

        this._container.appendChild(this._app.view);

    }

    /**
     * Angular lifecycle event - on init
     */
    public ngAfterViewInit()
    {
        // reserved for future use
    }

    /**
     * Create a PixiJS stage from the HTML Canvas reference or return null if unable to create
     */

    public get apps()
    {
        return this._app;
    }

    public get screenWidth(){
        return this.baseWidth;
    }

    public get screenHeight(){
        return this.baseHeight;
    }

}
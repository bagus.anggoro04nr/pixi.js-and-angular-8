var path = require('path');

module.exports = {
    context: __dirname,
    entry: {
        app: './app/src/main.js'
    },
    output:{
        path: path.resolve(__dirname, 'dist') + '/app',
        filename: '[name].js',
        publicPath: '/dist'
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js']
    },
    devtool: 'source-map',
    module: {
        rules: [{
            test: /\.tsx?$/,
            include: path.resolve(__dirname,'src'),
            exclude: /node_modules/
        }]
    },
    plugins: [

    ],
    node: {
        fs: 'empty'
    },
    externals: {
        oimo: 'OIMO', //or true
        cannon: 'CANNON' //or true
    },

}
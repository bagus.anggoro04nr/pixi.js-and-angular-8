import * as PIXI from 'pixi.js'

// create a texture from an image path
var texture = PIXI.Texture.from('public/images/panda1.png');
var carrotTex = PIXI.Texture.from('public/images/leaf1.png');

// create a new Sprite using the texture
var panda = new PIXI.Sprite(texture);
var container = new PIXI.Container();
var background = new PIXI.Graphics();

var bullets = [];
var bulletSpeed = 10/60;
var baseWidth = window.innerWidth;
var baseHeight = window.innerHeight;//Math.floor(window.screen.height);
var aspectRatio = baseWidth/baseHeight;

const app = new PIXI.Application({
    baseWidth,
    baseHeight,
    backgroundColor: 0xFFFFFF,
    antialias: true,
    // align: 'center',
    // position: 'absolute',
    transparent: true,
});

// app.view.style.position = 'absolute';
// app.view.style.left = Math.floor(baseWidth -(baseWidth/2))+'px';
// app.view.style.top = Math.floor(baseHeight -(baseHeight/2))+'px';

document.body.appendChild(app.view);

//Center container
// container.x = (baseWidth - container.width) / 2;
// container.y = (baseHeight - container.height) / 2;
// container.x = app.screen.width;//(app.screen.width - container.width) / 2;
// container.y = app.screen.height;//(app.screen.height - container.height) / 2;

// center the sprite's anchor point
panda.anchor.x = 0.5;
panda.anchor.y = 0.5;
// panda.anchor.set(0.5);

// move the sprite to the center of the screen
panda.x = baseWidth / 2;
panda.y = baseHeight / 2;
// panda.position.x = (baseWidth - (baseWidth / 2));
// panda.position.y = (baseHeight - (baseHeight / 2));


background.beginFill(0xF08080);
background.drawRect(0,0, baseWidth, baseHeight);
background.endFill();

app.renderer.resize(window.innerWidth, window.innerHeight);
app.renderer.autoResize = true;

app.stage.addChild(container);

// background.x = (app.screen.width - background.width) ;
// background.y = (app.screen.height - background.height) ;


container.addChild(background);

container.addChild(panda);

container.interactive = true;
container.hitArea = app.screen;
container.interactive = true;
container.on('mousemove', function(event) {
    panda.rotation = rotateToPoint(mousePosition.x, mousePosition.y, panda.position.x, panda.position.y);
});
container.on("mousedown", function(e){
    shoot(panda.rotation, {
        x: panda.position.x+Math.cos(panda.rotation)*20,
        y: panda.position.y+Math.sin(panda.rotation)*20
    });
});


function shoot(rotation, startPosition){
    var bullet = new PIXI.Sprite(carrotTex);
    bullet.position.x = startPosition.x;
    bullet.position.y = startPosition.y;
    bullet.rotation = rotation;
    container.addChild(bullet);
    bullets.push(bullet);
}

function rotateToPoint(mx, my, px, py){
    var self = this;
    var dist_Y = '';
    var dist_X = '';
    var angle = '';
    dist_Y = my - py;
    dist_X = mx - px;
    angle = Math.atan2(dist_Y,dist_X);
    //var degrees = angle * 180/ Math.PI;
    return angle;
}

var mousePosition = app.renderer.plugins.interaction.mouse.global;

// start animating
animate();

function animate() {
    requestAnimationFrame(animate);

    // just for fun, let's rotate mr rabbit a little
    panda.rotation = rotateToPoint(mousePosition.x, mousePosition.y, panda.position.x, panda.position.y);

    for(var b=bullets.length-1;b>=0;b--){
        bullets[b].position.x += Math.cos(bullets[b].rotation)*bulletSpeed;
        bullets[b].position.y += Math.sin(bullets[b].rotation)*bulletSpeed;
    }
    // render the container
    app.render(container);
}

// Listen for animate update
app.ticker.add(function(delta) {
    // just for fun, let's rotate mr rabbit a little
    // delta is 1 if running at 100% performance
    // creates frame-independent tranformation
    panda.x += Math.cos(panda.rotation) * delta;
    panda.y += Math.sin(panda.rotation) * delta;
});